describe("Get summary", () => {
  before(() => {
    cy.intercept("GET", "http://localhost:3000/project", {
      fixture: "project.json",
    });
    cy.visit("/summary");
  });

  afterEach(() => {
    cy.screenshot();
  });

  it("check legend", () => {
    cy.get("span[class='legend-label-text']").should((legendTexts) => {
      expect(legendTexts).to.have.length(2);
      expect(legendTexts[0]).to.contain("SPH-1");
      expect(legendTexts[1]).to.contain("ITN-3");
    });
  });

  it("check description pipe", () => {
    cy.get("p[id='description']").should((paragraphs) => {
      expect(paragraphs).to.have.length(3);
      expect(paragraphs[0]).to.contain("Project : Splash(SPH-1)");
      expect(paragraphs[0]).to.contain("Presale mandays usages : 11 day(s)");
      expect(paragraphs[0]).to.contain("Implement mandays usages : 11 day(s)");
      expect(paragraphs[1]).to.contain("Project : Itoen(ITN-3)");
      expect(paragraphs[1]).to.contain("Presale mandays usages : 22 day(s)");
      expect(paragraphs[1]).to.contain("Implement mandays usages : 22 day(s)");
      expect(paragraphs[2]).to.contain("Project : Speedwagon(SWG-888)");
      expect(paragraphs[2]).to.contain("Presale mandays usages : 0 day(s)");
      expect(paragraphs[2]).to.contain("Implement mandays usages : 0 day(s)");
    });
  });
});
