describe("Get history", () => {
  before(() => {
    cy.intercept("GET", "http://localhost:3000/scrum", {
      fixture: "scrum.json",
    });
    cy.visit("/history");
  });
  afterEach(() => {
    cy.screenshot();
  });

  it("load history, should be 3 cards in the list.", () => {
    cy.get("record-card").as("cards");
    cy.get("@cards").should((cards) => {
      expect(cards).to.have.length(3);
    });
  });
});
