describe("Add new record", () => {
  // Get input element
  function getElement() {
    cy.get("input[id='date']").as("inputDate");
    cy.get("select[id='type']").as("selectType");
    cy.get("select[id='project']").as("selectProject");
    cy.get("textarea[id='description']").as("textareaDescriptions");
    cy.get("button").contains("Save").as("saveButton");
  }

  beforeEach(() => {
    cy.intercept("*", (request) => {
      let url = request.url;
      let method = request.method;
      let result;
      if (url.includes("/project")) {
        switch (method) {
          case "GET": {
            result = { fixture: "project.json" };
            break;
          }

          case "POST": {
            break;
          }

          default:
            break;
        }
      } else if (url.includes("/scrum")) {
        switch (method) {
          case "GET": {
            break;
          }

          case "POST": {
            result = { fixture: "single-scrum.json" };
            break;
          }

          default:
            break;
        }
      }
      request.reply(result);
    });
  });

  afterEach(() => {
    cy.screenshot();
  });

  it("check error text, should be error text when fields were touched but no value.", () => {
    cy.visit("/new_record");
    getElement();
    // Trigger error messages
    cy.get("@inputDate").focus().blur();
    cy.get("@selectType").focus().blur();
    cy.get("@selectProject").focus().blur();
    cy.get("@textareaDescriptions").focus().blur();
    // Check error message
    cy.get("span[errorText]").then((errorTexts) => {
      expect(errorTexts).contain("Please fill date");
      expect(errorTexts).contain("Please select type");
      expect(errorTexts).contain("Please select project");
      expect(errorTexts).contain("Please fill information");
    });
  });

  it("fill information, should be value in all fields.", () => {
    getElement();
    // Fill information
    cy.get("@inputDate").type("2020-01-01");
    cy.get("@selectType").select("Implement");
    cy.get("@selectProject").select("Splash");
    cy.get("@textareaDescriptions").type(
      "Ora! Ora! Ora! Ora! Ora! Ora! Ora! Ora! Ora! Ora! Ora! Ora! Ora! Ora! Ora! "
    );
  });

  it("check error text and reset form, should be no values in all field", () => {
    // Get input element
    getElement();
    cy.get("span[errorText]").should("not.exist");
    cy.get("@saveButton").click();
    cy.get("@inputDate").should("be.empty");
    cy.get("@selectType").should("not.be.selected");
    cy.get("@selectProject").should("not.be.selected");
    cy.get("@textareaDescriptions").should("be.empty");
  });
});
