import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ProjectComponent } from "./feature/project/project.component";
import { AuthGuard } from "./guard/auth.guard";

const routes: Routes = [
  { path: "summary", loadChildren: () => import('./feature/summary/summary.module').then((m) => m.SummaryModule), },
  { path: "new_record", loadChildren: () => import('./feature/new_record/new-record.module').then((m) => m.NewRecordModule), },
  { path: "history", loadChildren: () => import('./feature/history/history.module').then((m) => m.HistoryModule), },
  { path: "project", component: ProjectComponent, canActivate: [AuthGuard] },
  { path: "**", redirectTo: "summary" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
