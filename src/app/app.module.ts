import { ProjectEffect } from './store/project/project.effect';
import * as fromApp from './store/app.reducer';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { AppComponent } from "./app.component";
import { NewRecordModule } from "./feature/new_record/new-record.module";
import { SummaryModule } from "./feature/summary/summary.module";
import { HistoryModule } from "./feature/history/history.module";
import { ProjectModule } from "./feature/project/project.module";
import { CoreModule } from "./core/core.module";
import { StoreModule } from "@ngrx/store";
import { EffectsModule } from '@ngrx/effects';
import { HttpClientModule } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { ScrumEffect } from './store/scrum/scrum.effect';

const effects = [
  ProjectEffect,
  ScrumEffect
];

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NewRecordModule,
    SummaryModule,
    HistoryModule,
    ProjectModule,
    CoreModule,
    HttpClientModule,
    StoreModule.forRoot(fromApp.appReducer),
    EffectsModule.forRoot(effects),
    StoreDevtoolsModule.instrument({ logOnly: environment.production }),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
