import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ErrorTextDirective } from "../directives/error-text.directive";
import { ReactiveFormsModule } from "@angular/forms";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatButtonModule } from "@angular/material/button";
import { MatDialogModule } from "@angular/material/dialog";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatTableModule } from "@angular/material/table";
import { HeaderComponent } from './header/header.component';

@NgModule({
  declarations: [ErrorTextDirective],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatButtonModule,
    MatDialogModule,
    MatTableModule,
    ReactiveFormsModule,
  ],
  exports: [
    ErrorTextDirective,
    CommonModule,
    MatFormFieldModule,
    MatButtonModule,
    MatDialogModule,
    MatTableModule,
    ReactiveFormsModule,
  ],
})
export class SharedModule {}
