import { ApiHttpService } from "../../service/api-http.service";
import { AuthService } from "./../../service/auth.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"],
})
export class HeaderComponent implements OnInit {
  constructor(private authService: AuthService) {}

  ngOnInit(): void {}

  onSudo() {
    // this.authService.sudo();
    // this.authService.testApi();
  }
}
