import { ValidatorFn, AbstractControl } from "@angular/forms";

/** A hero's name can't match the given regular expression */
export function forbiddenNameValidator(
  control: AbstractControl
): { [key: string]: any } | null {
  const forbidden = false;
  console.log("CONTROLLL :: ", control);
  return forbidden ? { forbiddenName: { value: control.value } } : null;
}
