import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

@Component({
  selector: "app-confirm-dialog",
  templateUrl: "./confirm-dialog.component.html",
  styleUrls: ["./confirm-dialog.component.css"],
})
export class ConfirmDialogComponent implements OnInit {
  dialogText: string = "example dialog text";
  dialogLeftBtnText: string = "Cancel";
  dialogRightBtnText: string = "Okay";
  dialogRightBtnCallback: Function = () => {};

  constructor(
    public dialogRef: MatDialogRef<ConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { dialogText: string; dialogRightBtnCallback: Function }
  ) {}

  ngOnInit(): void {
    this.dialogText = this.data.dialogText;
    this.dialogRightBtnCallback = this.data.dialogRightBtnCallback;
  }

  onCancelBtn() {
    this.dialogRef.close();
  }

  onOkayBtn() {
    this.dialogRightBtnCallback();
    this.dialogRef.close();
  }
}
