import { JsonPlaceholderModel } from "./../model/json-placeholder.model";
import { Injectable } from "@angular/core";
import { ApiHttpService } from "./api-http.service";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  private userIsAdmin: boolean = true;

  constructor(private http: ApiHttpService) {}

  public checkAuthed(): Promise<boolean> {
    return new Promise((resolve, _) => {
      if (this.userIsAdmin) {
        return resolve(true);
      } else {
        return resolve(false);
      }
    });
  }

  sudo() {
    this.userIsAdmin = !this.userIsAdmin;
    alert(
      this.userIsAdmin ? "sudo mode activated!!" : "sudo mode deactivated!!"
    );
  }
}
