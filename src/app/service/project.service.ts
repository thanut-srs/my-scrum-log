import {
  Project,
  ProjectAdapter,
  ProjectListAdapter,
} from "../model/project/project.model";
import { Subject } from "rxjs";
import { Injectable } from "@angular/core";
import { ApiHttpService } from "./api-http.service";
import { from } from "rxjs";
import { delay } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class ProjectService {
  projectSubject: Subject<any> = new Subject();

  constructor(
    private http: ApiHttpService,
    private projectAdapter: ProjectAdapter,
    private projectListAdapter: ProjectListAdapter
  ) {}

  getProjectsList() {
    return from(
      this.http
        .get("/project")
        .then((result) => {
          return this.projectListAdapter.adapt(result);
        })
        .catch((error) => {
          throw new Error(error);
        })
    ).pipe(delay(1000));
  }

  addNewProject(data: Project) {
    return from(
      this.http
        .post("/project", data)
        .then((result) => {
          return this.projectAdapter.adapt(result);
        })
        .catch((error) => {
          throw new Error(error);
        })
    );
  }
}
