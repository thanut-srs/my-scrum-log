import { environment } from "./../../environments/environment";
import { Constants } from "./../config/constant";
// Angular Modules
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import axios, { AxiosError } from "axios";

@Injectable()
export class ApiHttpService {
  constructor(
    // Uncomment if using http client
    // private http: HttpClient,
    private constants: Constants
  ) {
    // Config request interceptors
    axios.interceptors.request.use((config) => {
      config.headers = { jojo: "aiyahhh" };
      let endpoint = environment.production
        ? this.constants.API_ENDPOINT
        : this.constants.API_MOCK_ENDPOINT;
      config.url = endpoint + config.url;
      console.log("[REQ][URL] : ", config.url);
      return config;
    });

    //Config response interceptors
    axios.interceptors.response.use(
      (response) => {
        console.log("[RES][SUCCES] : ", response);
        return response.data;
      },
      (error) => {
        console.log("[RES][ERROR] : ", error);
        return Promise.reject(error);
      }
    );
  }
  public get(url: string, options?: any) {
    return axios.get(url, options);
  }
  public post(url: string, data: any, options?: any) {
    return axios.post(url, data, options);
  }
  public put(url: string, data: any, options?: any) {
    return axios.put(url, data, options);
  }
  public delete(url: string, options?: any) {
    return axios.delete(url, options);
  }
}
