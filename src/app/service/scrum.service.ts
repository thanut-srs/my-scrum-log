import { delay } from "rxjs/operators";
import { ScrumAdapter, ScrumListAdapter } from "../model/scrum/scrum.model";
import { Scrum } from "src/app/model/scrum/scrum.model";
import { Injectable } from "@angular/core";
import { from } from "rxjs";
import { ApiHttpService } from "./api-http.service";
@Injectable({
  providedIn: "root",
})
export class ScrumService {
  constructor(
    private http: ApiHttpService,
    private scrumAdapter: ScrumAdapter,
    private scrumListAdapter: ScrumListAdapter
  ) {}

  getScrumLog() {
    // let userId = 1;
    return from(
      this.http
        .get("/scrum")
        .then((result) => {
          return this.scrumListAdapter.adapt(result);
        })
        .catch((error) => {
          throw new Error(error);
        })
    ).pipe(delay(1000));
  }

  addNewRecord(data: Scrum) {
    const body = {
      date: data.date,
      description: data.description,
      project: data.project,
      project_code: data.project_code,
      type: data.type,
    };
    console.log("TEST 2", body);
    return from(
      this.http
        .post("/scrum", body)
        .then((_) => {
          console.log("TEST", _);
          return this.scrumAdapter.adapt(data);
        })
        .catch((error) => {
          console.log("error", error);
          throw new Error(error);
        })
    );
  }

  editRecord(data: Scrum) {
    const body = {
      date: data.date,
      description: data.description,
      project: data.project,
      project_code: data.project_code,
      type: data.type,
    };
    return from(
      this.http
        .put(`/scrum/${data.id}`, body)
        .then((result) => {
          return this.scrumAdapter.adapt(result);
        })
        .catch((error) => {
          throw new Error(error);
        })
    );
  }

  deleteRecord(scrum: Scrum) {
    // let userId = 1;
    return from(
      this.http
        .delete(`/scrum/${scrum.id}`)
        .then((_) => {
          return true;
        })
        .catch((error) => {
          throw new Error(error);
        })
    );
  }
}
