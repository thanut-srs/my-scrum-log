import { ActionReducerMap } from "@ngrx/store";
import * as fromProject from "./project/project.reducer";
import * as fromScrum from "./scrum/scrum.reducer";

export interface AppState {
  project: fromProject.ProjectState;
  scrum: fromScrum.ScrumState;
}

export const appReducer: ActionReducerMap<AppState> = {
  project: fromProject.ProjectReducer,
  scrum: fromScrum.ScrumReducer,
};
