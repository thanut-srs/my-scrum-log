import { ScrumActionTypes } from "./scrum.action";
import { Scrum } from "src/app/model/scrum/scrum.model";

export interface ScrumState {
  list: Scrum[];
  loading: boolean;
  error: boolean;
}

const initialState: ScrumState = {
  list: [],
  loading: false,
  error: false,
};

export function ScrumReducer(state: ScrumState = initialState, action: any) {
  switch (action.type) {
    case ScrumActionTypes.LOAD_SCRUM:
      return { ...state, list: [], loading: true, error: false };

    case ScrumActionTypes.LOAD_SCRUM_SUCCESS:
      return {
        ...state,
        list: action.payload,
        loading: false,
        error: false,
      };

    case ScrumActionTypes.LOAD_SCRUM_FAILED:
      return { ...state, loading: false, error: true };

    case ScrumActionTypes.ADD_SCRUM_SUCCESS:
      return { ...state, list: [...state.list, action.payload] };

    case ScrumActionTypes.ADD_SCRUM_FAILED:
      alert("Add Failed");
      return {
        ...state,
        error: true,
      };

    case ScrumActionTypes.DELETE_SCRUM_SUCCESS: {
      // Can't manipulate data from state directly na sja
      let tmpList = Array.from(state.list);
      let index: number = tmpList.findIndex(
        (scrum) => scrum.id == action.payload.id
      );
      if (index != -1) {
        tmpList.splice(index, 1);
        return { ...state, list: tmpList };
      }
      return { ...state };
    }

    case ScrumActionTypes.DELETE_SCRUM_FAILED:
      alert("Delete Failed");
      return {
        ...state,
      };

    case ScrumActionTypes.EDIT_SCRUM_SUCCESS: {
      // Can't manipulate data from state directly na ja
      let tmpList = Array.from(state.list);
      let edittedScrum = action.payload;
      let index: number = tmpList.findIndex(
        (scrum) => scrum.id == edittedScrum.id
      );
      if (index != -1) {
        tmpList[index] = edittedScrum;
        return { ...state, list: tmpList };
      }
      return { ...state };
    }

    case ScrumActionTypes.EDIT_SCRUM_FAILED:
      alert("Edit Failed");
      return {
        ...state,
      };

    default:
      return state;
  }
}
