import { ScrumService } from "./../../service/scrum.service";
import {
  ScrumActionTypes,
  LoadScrumAction,
  LoadScrumSuccessAction,
  LoadScrumFailedAction,
  AddScrumAction,
  AddScrumSuccessAction,
  AddScrumFailedAction,
  DeleteScrumAction,
  DeleteScrumSuccessAction,
  DeleteScrumFailedAction,
  EditScrumAction,
  EditScrumFailedAction,
  EditScrumSuccessAction,
} from "./scrum.action";
import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { Store } from "@ngrx/store";
import { of } from "rxjs";
import { switchMap, map, catchError, withLatestFrom } from "rxjs/operators";
import { AppState } from "../app.reducer";
import { Scrum } from "src/app/model/scrum/scrum.model";

@Injectable()
export class ScrumEffect {
  constructor(
    private action$: Actions,
    private scrumService: ScrumService,
    private store: Store<AppState>
  ) {}

  loadScrum$ = createEffect(() =>
    this.action$.pipe(
      ofType<LoadScrumAction>(ScrumActionTypes.LOAD_SCRUM),
      switchMap(() =>
        this.scrumService.getScrumLog().pipe(
          map((result) => {
            let data: Scrum[] = result.scrumList;
            return new LoadScrumSuccessAction(data);
          }),
          catchError((_) => of(new LoadScrumFailedAction()))
        )
      )
    )
  );

  addScrum$ = createEffect(() =>
    this.action$.pipe(
      ofType<AddScrumAction>(ScrumActionTypes.ADD_SCRUM),
      withLatestFrom(this.store.select("scrum")),
      switchMap(([action, _]) =>
        this.scrumService.addNewRecord(action.payload).pipe(
          map((result) => {
            alert("Add success");
            return new AddScrumSuccessAction(result);
          }),
          catchError((_) => of(new AddScrumFailedAction()))
        )
      )
    )
  );

  deleteScrum$ = createEffect(() =>
    this.action$.pipe(
      ofType<DeleteScrumAction>(ScrumActionTypes.DELETE_SCRUM),
      withLatestFrom(this.store.select("scrum")),
      switchMap(([action, _]) =>
        this.scrumService.deleteRecord(action.payload).pipe(
          map((_) => {
            alert("Delete success");
            return new DeleteScrumSuccessAction(action.payload);
          }),
          catchError((_) => of(new DeleteScrumFailedAction()))
        )
      )
    )
  );

  editScrum$ = createEffect(() =>
    this.action$.pipe(
      ofType<EditScrumAction>(ScrumActionTypes.EDIT_SCRUM),
      withLatestFrom(this.store.select("scrum")),
      switchMap(([action, _]) =>
        this.scrumService.editRecord(action.payload).pipe(
          map((result) => {
            alert("Edit success");
            return new EditScrumSuccessAction(result);
          }),
          catchError((_) => of(new EditScrumFailedAction()))
        )
      )
    )
  );
}
