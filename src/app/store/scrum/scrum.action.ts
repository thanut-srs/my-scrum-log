import { Action } from "@ngrx/store";
import { Scrum } from "src/app/model/scrum/scrum.model";

export enum ScrumActionTypes {
  LOAD_SCRUM = "[SCRUM] Load scrum",
  LOAD_SCRUM_SUCCESS = "[SCRUM] Load scrum success",
  LOAD_SCRUM_FAILED = "[SCRUM] Load scrum failed",
  ADD_SCRUM = "[SCRUM] Add scrum",
  ADD_SCRUM_SUCCESS = "[SCRUM] Add scrum success",
  ADD_SCRUM_FAILED = "[SCRUM] Add scrum failed",
  DELETE_SCRUM = "[SCRUM] Delete scrum",
  DELETE_SCRUM_SUCCESS = "[SCRUM] Delete scrum success",
  DELETE_SCRUM_FAILED = "[SCRUM] Delete scrum failed",
  EDIT_SCRUM = "[SCRUM] Edit scrum",
  EDIT_SCRUM_SUCCESS = "[SCRUM] Edit scrum success",
  EDIT_SCRUM_FAILED = "[SCRUM] Edit scrum failed",
}

export class LoadScrumAction implements Action {
  readonly type = ScrumActionTypes.LOAD_SCRUM;
}

export class LoadScrumSuccessAction implements Action {
  readonly type = ScrumActionTypes.LOAD_SCRUM_SUCCESS;

  constructor(public payload: Scrum[]) {}
}

export class LoadScrumFailedAction implements Action {
  readonly type = ScrumActionTypes.LOAD_SCRUM_FAILED;
}

export class AddScrumAction implements Action {
  readonly type = ScrumActionTypes.ADD_SCRUM;
  
  constructor(public payload: Scrum) {}
}

export class AddScrumSuccessAction implements Action {
  readonly type = ScrumActionTypes.ADD_SCRUM_SUCCESS;

  constructor(public payload: Scrum) {}
}

export class AddScrumFailedAction implements Action {
  readonly type = ScrumActionTypes.ADD_SCRUM_FAILED;
}

export class DeleteScrumAction implements Action {
  readonly type = ScrumActionTypes.DELETE_SCRUM;
  
  constructor(public payload: Scrum) {}
}

export class DeleteScrumSuccessAction implements Action {
  readonly type = ScrumActionTypes.DELETE_SCRUM_SUCCESS;

  constructor(public payload: Scrum) {}
}

export class DeleteScrumFailedAction implements Action {
  readonly type = ScrumActionTypes.DELETE_SCRUM_FAILED;
}

export class EditScrumAction implements Action {
  readonly type = ScrumActionTypes.EDIT_SCRUM;
  
  constructor(public payload: Scrum) {}
}

export class EditScrumSuccessAction implements Action {
  readonly type = ScrumActionTypes.EDIT_SCRUM_SUCCESS;

  constructor(public payload: Scrum) {}
}

export class EditScrumFailedAction implements Action {
  readonly type = ScrumActionTypes.EDIT_SCRUM_FAILED;
}

export type ProjectsAction =
  | LoadScrumAction
  | LoadScrumSuccessAction
  | LoadScrumFailedAction
  | AddScrumAction
  | AddScrumSuccessAction
  | AddScrumFailedAction;
