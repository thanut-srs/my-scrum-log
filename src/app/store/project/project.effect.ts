import { of } from "rxjs";
import {
  AddProjectAction,
  AddProjectFailedAction,
  AddProjectSuccessAction,
  LoadProjectAction,
  LoadProjectFailedAction,
  LoadProjectSuccesAction,
  ProjectActionTypes,
} from "./project.actions";
import { ProjectService } from "src/app/service/project.service";
import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { map, switchMap, withLatestFrom, catchError } from "rxjs/operators";
import { Project } from "src/app/model/project/project.model";
import { AppState } from "../app.reducer";
import { Store } from "@ngrx/store";

@Injectable()
export class ProjectEffect {
  constructor(
    private action$: Actions,
    private projectService: ProjectService,
    private store: Store<AppState>
  ) {}

  loadProjects$ = createEffect(() =>
    this.action$.pipe(
      ofType<LoadProjectAction>(ProjectActionTypes.LOAD_PROJECT),
      switchMap(() =>
        this.projectService.getProjectsList().pipe(
          map((result) => {
            let data: Project[] = result.projectList;
            return new LoadProjectSuccesAction(data);
          }),
          catchError((_) => of(new LoadProjectFailedAction()))
        )
      )
    )
  );

  addProject$ = createEffect(() =>
    this.action$.pipe(
      ofType<AddProjectAction>(ProjectActionTypes.ADD_PROJECT),
      withLatestFrom(this.store.select("project")),
      switchMap(([action, _]) =>
        this.projectService.addNewProject(action.payload).pipe(
          map((result) => {
            let data: Project = result;
            alert("Add success!");
            return new AddProjectSuccessAction(data);
          }),
          catchError((_) => {
            alert("Add failed!");
            return of(new AddProjectFailedAction());
          })
        )
      )
    )
  );
}
