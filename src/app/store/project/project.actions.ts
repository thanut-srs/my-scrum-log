import { Project } from "../../model/project/project.model";
import { Action } from "@ngrx/store";

export enum ProjectActionTypes {
  LOAD_PROJECT = "[PROJECT] Load project",
  LOAD_PROJECT_SUCCESS = "[PROJECT] Load project success",
  LOAD_PROJECT_FAILED = "[PROJECT] Load project failed",
  ADD_PROJECT = "[PROJECT] ADD project",
  ADD_PROJECT_SUCCESS = "[PROJECT] ADD project success",
  ADD_PROJECT_FAILED = "[PROJECT] ADD project failed",
}

export class LoadProjectAction implements Action {
  readonly type = ProjectActionTypes.LOAD_PROJECT;
}

export class LoadProjectSuccesAction implements Action {
  readonly type = ProjectActionTypes.LOAD_PROJECT_SUCCESS;

  constructor(public payload: Project[]) {}
}

export class LoadProjectFailedAction implements Action {
  readonly type = ProjectActionTypes.LOAD_PROJECT_FAILED;
}

export class AddProjectAction implements Action {
  readonly type = ProjectActionTypes.ADD_PROJECT;

  constructor(public payload: Project) {}
}

export class AddProjectSuccessAction implements Action {
  readonly type = ProjectActionTypes.ADD_PROJECT_SUCCESS;

  constructor(public payload: Project) {}
}

export class AddProjectFailedAction implements Action {
  readonly type = ProjectActionTypes.ADD_PROJECT_FAILED;
}

export type ProjectsAction =
  | LoadProjectAction
  | LoadProjectSuccesAction
  | LoadProjectFailedAction
  | AddProjectAction
  | AddProjectSuccessAction
  | AddProjectFailedAction;
