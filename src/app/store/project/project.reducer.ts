import { ProjectActionTypes } from "./project.actions";
import { Project } from "../../model/project/project.model";

export interface ProjectState {
  list: Project[];
  loading: boolean;
  error: boolean;
}

const initialState: ProjectState = {
  list: [], 
  loading: false,
  error: false,
};

export function ProjectReducer(
  state: ProjectState = initialState,
  action: any
) {
  switch (action.type) {
    case ProjectActionTypes.LOAD_PROJECT:
      return { ...state, list: [], loading: true };

    case ProjectActionTypes.LOAD_PROJECT_SUCCESS:
      return {
        ...state,
        list: action.payload,
        loading: false,
      };

    case ProjectActionTypes.LOAD_PROJECT_FAILED:
      return { ...state, loading: false, error: true };

    case ProjectActionTypes.ADD_PROJECT_SUCCESS:
      return {
        ...state,
        list: [...state.list, action.payload],
        loading: false,
        error: false,
      };

    case ProjectActionTypes.ADD_PROJECT_FAILED:
      return {
        ...state,
      };

    default:
      return state;
  }
}
