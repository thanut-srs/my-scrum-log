import { Pipe, PipeTransform } from "@angular/core";
import { Project } from "../model/project/project.model";

@Pipe({
  name: "mandaysDescriptionPipe",
})
export class SummaryMandaysDescriptionPipe implements PipeTransform {
  transform(projectData: Project): string {
    return `
    Project : ${projectData.name}(${projectData.code}), 
    Presale mandays usages : ${projectData.pMandays} day(s)
    Implement mandays usages : ${projectData.iMandays} day(s)
    `;
  }
}
