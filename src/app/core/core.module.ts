import { Constants } from '../config/constant';
import { MatButtonModule } from "@angular/material/button";
import { HeaderComponent } from "../shared/header/header.component";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatToolbarModule } from "@angular/material/toolbar";
import { AppRoutingModule } from "../app-routing.module";
import { ApiHttpService } from '../service/api-http.service';

@NgModule({
  declarations: [HeaderComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    MatToolbarModule,
    MatSidenavModule,
    MatButtonModule,
    
  ],
  exports: [
    MatToolbarModule,
    MatSidenavModule,
    MatButtonModule,
    AppRoutingModule,
    HeaderComponent,
  ],
  providers: [Constants, ApiHttpService],
})
export class CoreModule {}
