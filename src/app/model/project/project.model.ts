import { Injectable } from "@angular/core";
import { Adapter } from "../../core/adapter";

export class Project {
  constructor(
    public id: number,
    public name: string,
    public code: string,
    public iMandays: number,
    public pMandays: number
  ) {}
}

export class ProjectList {
  constructor(public projectList: Array<Project>) {}
}

@Injectable({
  providedIn: "root",
})
export class ProjectAdapter implements Adapter<Project> {
  adapt(item: any): Project {
    return new Project(
      item.id,
      item.name,
      item.code,
      item.iMandays,
      item.pMandays
    );
  }
}

@Injectable({
  providedIn: "root",
})
export class ProjectListAdapter implements Adapter<ProjectList> {
  constructor(private adapter: ProjectAdapter) {}
  adapt(list: any): ProjectList {
    const data = list.map((project: Project) => {
      return this.adapter.adapt(project);
    });
    return new ProjectList(data);
  }
}
