import { Injectable } from "@angular/core";
import { Adapter } from "../../core/adapter";

export class Scrum {
  constructor(
    public id: number,
    public date: string,
    public description: string,
    public type: string,
    public project: string,
    public project_code: string
  ) {}
}

@Injectable({
  providedIn: "root",
})
export class ScrumAdapter implements Adapter<Scrum> {
  adapt(item: any): Scrum {
    return new Scrum(
      item.id,
      item.date,
      item.description,
      item.type,
      item.project,
      item.project_code
    );
  }
}

export class ScrumList {
  constructor(public scrumList: Scrum[]) {}
}

@Injectable({
  providedIn: "root",
})
export class ScrumListAdapter implements Adapter<ScrumList> {
  constructor(private adapter: ScrumAdapter) {}
  adapt(list: any): ScrumList {
    const data = list.map((scrum: Scrum) => {
      return this.adapter.adapt(scrum);
    });
    return new ScrumList(data);
  }
}
