import { LoadScrumAction } from "./../../../store/scrum/scrum.action";
import { Store } from "@ngrx/store";
import { Component, Inject, OnInit } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Scrum } from "src/app/model/scrum/scrum.model";
import { ScrumService } from "src/app/service/scrum.service";
import { EditDialogComponent } from "../edit-dialog/edit-dialog.component";
import { AppState } from "src/app/store/app.reducer";
import { DeleteScrumAction } from "src/app/store/scrum/scrum.action";

@Component({
  selector: "app-delete-confirm-dialog",
  templateUrl: "./delete-confirm-dialog.component.html",
  styleUrls: ["./delete-confirm-dialog.component.css"],
})
export class DeleteConfirmDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<EditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { scrum: Scrum },
    private store: Store<AppState>
  ) {
    console.log("GOING TO DELETE :: ",this.data.scrum.id);
  }

  onDelete() {
    this.store.dispatch(new DeleteScrumAction(this.data.scrum));
    this.dialogRef.close();
  }

  onCancel() {
    this.dialogRef.close();
  }
}
