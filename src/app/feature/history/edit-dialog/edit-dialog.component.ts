import { EditScrumAction } from "./../../../store/scrum/scrum.action";
import { Store } from "@ngrx/store";
import { ConfirmDialogComponent } from "../../../component/confirm-dialog/confirm-dialog.component";
import { ProjectService } from "../../../service/project.service";
import { ScrumService } from "../../../service/scrum.service";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Inject, OnDestroy } from "@angular/core";
import { Component } from "@angular/core";
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from "@angular/material/dialog";
import { Scrum } from "src/app/model/scrum/scrum.model";
import { Project } from "src/app/model/project/project.model";
import { AppState } from "src/app/store/app.reducer";
import { LoadProjectAction } from "src/app/store/project/project.actions";
import { Subscription } from "rxjs";

@Component({
  selector: "app-edit-dialog",
  templateUrl: "./edit-dialog.component.html",
  styleUrls: ["./edit-dialog.component.css"],
})
export class EditDialogComponent implements OnDestroy {
  scrumForm: FormGroup;
  projectsList: Array<Project> = [];
  storeSubscription: Subscription;

  constructor(
    private dialogRef: MatDialogRef<EditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { scrum: Scrum },
    private store: Store<AppState>,
    private matDialog: MatDialog
  ) {
    this.scrumForm = new FormGroup({
      date: new FormControl(data.scrum.date, [Validators.required]),
      type: new FormControl(data.scrum.type, [Validators.required]),
      project: new FormControl(data.scrum.project, [Validators.required]),
      description: new FormControl(data.scrum.description, [
        Validators.required,
      ]),
    });
    this.storeSubscription = this.store.select("project").subscribe((state) => {
      this.projectsList = state.list;
    });
    this.store.dispatch(new LoadProjectAction());
  }
  ngOnDestroy(): void {
    this.storeSubscription.unsubscribe();
  }

  onSubmit() {
    this.matDialog.open(ConfirmDialogComponent, {
      width: "300px",
      height: "150px",
      data: {
        dialogText: "Are you sure?",
        dialogRightBtnCallback: () => this.editData(),
      },
    });
  }

  editData() {
    let newData: Scrum = {
      id: this.data.scrum.id,
      date: this.scrumForm.get("date")?.value,
      type: this.scrumForm.get("type")?.value,
      project: this.scrumForm.get("project")?.value,
      project_code: "",
      description: this.scrumForm.get("description")?.value,
    };
    this.store.dispatch(new EditScrumAction(newData));
    this.dialogRef.close();
  }
}
