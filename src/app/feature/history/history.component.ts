import { LoadScrumAction } from "./../../store/scrum/scrum.action";
import { Store } from "@ngrx/store";
import { ScrumService } from "./../../service/scrum.service";

import { Component, OnDestroy, OnInit } from "@angular/core";
import { Scrum } from "src/app/model/scrum/scrum.model";
import { AppState } from "src/app/store/app.reducer";
import { Subscription } from "rxjs";

@Component({
  selector: "app-history",
  templateUrl: "./history.component.html",
  styleUrls: ["./history.component.css"],
})
export class HistoryComponent implements OnDestroy {
  scrumData: Array<Scrum> = [];
  hasError: boolean = false;
  isLoading: boolean = false;
  storeSubscription: Subscription;

  constructor(private store: Store<AppState>) {
    this.storeSubscription = this.store.select("scrum").subscribe((state) => {
      this.scrumData = state.list;
      this.hasError = state.error;
      this.isLoading = state.loading;
    });
    this.store.dispatch(new LoadScrumAction());
  }

  ngOnDestroy(): void {
    this.storeSubscription.unsubscribe();
  }
}
