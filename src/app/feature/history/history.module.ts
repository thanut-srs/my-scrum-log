import { SharedModule } from './../../shared/shared.module';
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ConfirmDialogComponent } from "src/app/component/confirm-dialog/confirm-dialog.component";
import { DeleteConfirmDialogComponent } from "src/app/feature/history/delete-confirm-dialog/delete-confirm-dialog.component";
import { EditDialogComponent } from "src/app/feature/history/edit-dialog/edit-dialog.component";
import { RecordCardComponent } from "src/app/feature/history/record-card/record-card.component";
import { HistoryComponent } from "./history.component";
import { RouterModule, Routes } from "@angular/router";

const historyRoutes: Routes = [{ path: "", component: HistoryComponent }];

@NgModule({
  declarations: [
    HistoryComponent,
    RecordCardComponent,
    EditDialogComponent,
    DeleteConfirmDialogComponent,
    ConfirmDialogComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(historyRoutes),
  ],
  exports: [],
  providers: [],
})
export class HistoryModule {}
