import { EditProjectDialogComponent } from "./edit-project-dialog/edit-project-dialog.component";
import { ProjectService } from "../../service/project.service";
import { Component, OnInit } from "@angular/core";
import { Project } from "src/app/model/project/project.model";
import { NewProjectInputComponent } from "src/app/feature/summary/new-project-dialog/new-project-input.component";
import { MatDialog } from "@angular/material/dialog";

@Component({
  selector: "app-project",
  templateUrl: "./project.component.html",
  styleUrls: ["./project.component.css"],
})
export class ProjectComponent implements OnInit {
  dataSource: Array<Project> = [];
  displayedColumns: string[] = ["name", "code", "iManday", "pManday", "action"];

  constructor(private dialog: MatDialog) {}

  ngOnInit(): void {}

  onEdit(project: Project): void {
    this.dialog.open(EditProjectDialogComponent, {
      height: "280px",
      width: "400px",
      data: {
        name: project.name,
        code: project.code,
      },
    });
  }
  onDelete(): void {}
}
