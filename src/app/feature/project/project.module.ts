import { SharedModule } from './../../shared/shared.module';
import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { EditProjectDialogComponent } from "src/app/feature/project/edit-project-dialog/edit-project-dialog.component";
import { ProjectComponent } from "./project.component";
import { Routes } from "@angular/router";

const projectRoutes: Routes = [{ path: "", component: ProjectComponent }];

@NgModule({
  declarations: [ProjectComponent, EditProjectDialogComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(projectRoutes),
  ],
  exports: [],
  providers: [],
})
export class ProjectModule {}
