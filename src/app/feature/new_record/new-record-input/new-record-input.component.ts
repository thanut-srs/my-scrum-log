import { Subscription } from "rxjs";
import { AddScrumAction } from "./../../../store/scrum/scrum.action";
import { Scrum } from "src/app/model/scrum/scrum.model";
import { Project } from "../../../model/project/project.model";

import { Component, OnDestroy } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { AppState } from "src/app/store/app.reducer";
import { Store } from "@ngrx/store";
import { LoadProjectAction } from "src/app/store/project/project.actions";
import { forbiddenNameValidator } from "src/app/shared/example.directives";

@Component({
  selector: "app-new-record-input",
  templateUrl: "./new-record-input.component.html",
  styleUrls: ["./new-record-input.component.css"],
})
export class NewRecordInputComponent implements OnDestroy {
  scrumForm: FormGroup;
  loadingProject = false;
  projectsList: Array<Project> = [];
  storeSubscription: Subscription;

  constructor(private store: Store<AppState>) {
    this.scrumForm = new FormGroup({
      date: new FormControl(null, [Validators.required]),
      type: new FormControl("", [Validators.required]),
      project: new FormControl("", [Validators.required]),
      description: new FormControl(null, Validators.required),
    });
    this.storeSubscription = this.store.select("project").subscribe((state) => {
      this.loadingProject = state.loading;
      this.projectsList = state.list;
    });
    this.store.dispatch(new LoadProjectAction());
  }

  onSubmit() {
    let data: Scrum = {
      id: 0,
      date: this.scrumForm.get("date")?.value,
      type: this.scrumForm.get("type")?.value,
      project: this.scrumForm.get("project")?.value,
      project_code: "",
      description: this.scrumForm.get("description")?.value,
    };
    this.store.dispatch(new AddScrumAction(data));
    this.scrumForm.reset();
  }

  ngOnDestroy(): void {
    this.storeSubscription.unsubscribe();
  }

  onChange() {
    console.log(this.scrumForm);
  }
}
