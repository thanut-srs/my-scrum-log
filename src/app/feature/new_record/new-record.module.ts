import { NewRecordInputComponent } from "src/app/feature/new_record/new-record-input/new-record-input.component";
import { SharedModule } from "./../../shared/shared.module";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { NewRecordComponent } from "./new-record.component";
import { RouterModule, Routes } from "@angular/router";

const newRecordRoute: Routes = [{ path: "", component: NewRecordComponent }];

@NgModule({
  declarations: [NewRecordComponent, NewRecordInputComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(newRecordRoute)
  ],
  exports: [],
})
export class NewRecordModule {}
