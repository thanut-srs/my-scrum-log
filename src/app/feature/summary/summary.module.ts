import { ProjectAdapter, ProjectListAdapter } from '../../model/project/project.model';
import { SharedModule } from './../../shared/shared.module';
import { Routes } from "@angular/router";
import { RouterModule } from "@angular/router";
import { SummaryMandaysDescriptionPipe } from "src/app/pipe/mandays-description.pipe";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SummaryComponent } from "./summary.component";
import { NgxChartsModule } from "@swimlane/ngx-charts";
import { NewProjectInputComponent } from "src/app/feature/summary/new-project-dialog/new-project-input.component";

const summaryRoutes: Routes = [{ path: "", component: SummaryComponent }];

@NgModule({
  declarations: [
    SummaryComponent,
    SummaryMandaysDescriptionPipe,
    NewProjectInputComponent,
  ],
  imports: [
    CommonModule,
    NgxChartsModule,
    SharedModule,
    RouterModule.forChild(summaryRoutes),
  ],
  exports: [],
})
export class SummaryModule {}
