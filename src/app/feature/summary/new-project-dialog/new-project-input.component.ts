import { ProjectService } from "../../../service/project.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Component, OnInit } from "@angular/core";
import { Project } from "src/app/model/project/project.model";
import { MatDialogRef } from "@angular/material/dialog";
import { AppState } from "src/app/store/app.reducer";
import { Store } from "@ngrx/store";
import { AddProjectAction } from "src/app/store/project/project.actions";

@Component({
  selector: "app-new-project-input",
  templateUrl: "./new-project-input.component.html",
  styleUrls: ["./new-project-input.component.css"],
})
export class NewProjectInputComponent {
  newProjectFormGroup: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<NewProjectInputComponent>,
    private store: Store<AppState>
  ) {
    this.newProjectFormGroup = new FormGroup({
      name: new FormControl(null, Validators.required),
      code: new FormControl(null, Validators.required),
    });
  }

  async onSubmit() {
    let name = this.newProjectFormGroup.get("name")?.value;
    let code = this.newProjectFormGroup.get("code")?.value;
    let data: Project = {
      id: 0,
      name: name,
      code: code,
      iMandays: 0,
      pMandays: 0,
    };
    this.store.dispatch(new AddProjectAction(data));
    this.newProjectFormGroup.reset();
  }
}
