import { LoadProjectAction } from "./../../store/project/project.actions";
import { Project } from "src/app/model/project/project.model";
import { Component, OnDestroy } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { NewProjectInputComponent } from "src/app/feature/summary/new-project-dialog/new-project-input.component";
import { Store } from "@ngrx/store";
import { AppState } from "src/app/store/app.reducer";
import { Subscription } from "rxjs";

@Component({
  selector: "app-summary",
  templateUrl: "summary.component.html",
  styleUrls: ["summary.component.css"],
})
export class SummaryComponent implements OnDestroy {
  projectData: Array<{ name: string; value: number }> = [];
  projectList: Array<Project> = [];
  isLoading: boolean = false;
  hasError: boolean = false;
  storeSubscription: Subscription;

  constructor(private dialog: MatDialog, private store: Store<AppState>) {
    this.storeSubscription = this.store.select("project").subscribe((state) => {
      this.isLoading = state.loading;
      this.hasError = state.error;
      this.projectList = state.list;
      this.getProjectList();
    });
    this.store.dispatch(new LoadProjectAction());
  }

  ngOnDestroy(): void {
    this.storeSubscription.unsubscribe();
  }

  getProjectList() {
    this.projectData = this.projectList
      .filter((project) => {
        return project.iMandays + project.pMandays > 0;
      })
      .map((val) => {
        return {
          name: val.code,
          value: val.iMandays + val.pMandays,
        };
      });
  }

  onNewProject() {
    this.dialog.open(NewProjectInputComponent, {
      height: "280px",
      width: "400px",
    });
  }
}
